package nl.robertvankammen.npcspawnplugin;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public final class NpcData implements ConfigurationSerializable {
  private final double x;
  private final double y;
  private final double z;
  private final String skinValue;
  private final String skinSignature;
  private final String naam;
  private final String server;

  public NpcData(double x, double y, double z, String skinValue, String skinSignature, String naam, String server) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.skinValue = skinValue;
    this.skinSignature = skinSignature;
    this.naam = naam;
    this.server = server;
  }

  public NpcData(Map<String, Object> data) {
    this((Double) data.get("x"),
      (Double) data.get("y"),
      (Double) data.get("z"),
      (String) data.get("skinValue"),
      (String) data.get("skinSignature"),
      (String) data.get("naam"),
      (String) data.get("server"));
  }

  static {
    ConfigurationSerialization.registerClass(NpcData.class);
  }

  @Override
  public @NotNull Map<String, Object> serialize() {
    Map<String, Object> data = new HashMap<>();
    data.put("x", x);
    data.put("y", y);
    data.put("z", z);
    data.put("skinValue", skinValue);
    data.put("skinSignature", skinSignature);
    data.put("naam", naam);
    data.put("server", server);
    return data;
  }

  public double x() {
    return x;
  }

  public double y() {
    return y;
  }

  public double z() {
    return z;
  }

  public String skinValue() {
    return skinValue;
  }

  public String skinSignature() {
    return skinSignature;
  }

  public String naam() {
    return naam;
  }

  public String server() {
    return server;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) return true;
    if (obj == null || obj.getClass() != this.getClass()) return false;
    var that = (NpcData) obj;
    return Double.doubleToLongBits(this.x) == Double.doubleToLongBits(that.x) &&
      Double.doubleToLongBits(this.y) == Double.doubleToLongBits(that.y) &&
      Double.doubleToLongBits(this.z) == Double.doubleToLongBits(that.z) &&
      Objects.equals(this.skinValue, that.skinValue) &&
      Objects.equals(this.skinSignature, that.skinSignature) &&
      Objects.equals(this.naam, that.naam) &&
      Objects.equals(this.server, that.server);
  }

  @Override
  public int hashCode() {
    return Objects.hash(x, y, z, skinValue, skinSignature, naam, server);
  }

  @Override
  public String toString() {
    return "NpcData[" +
      "x=" + x + ", " +
      "y=" + y + ", " +
      "z=" + z + ", " +
      "skinValue=" + skinValue + ", " +
      "skinSignature=" + skinSignature + ", " +
      "naam=" + naam + ", " +
      "server=" + server + ']';
  }

}
