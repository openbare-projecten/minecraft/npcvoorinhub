package nl.robertvankammen.npcspawnplugin;

import com.destroystokyo.paper.event.player.PlayerUseUnknownEntityEvent;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.minecraft.core.BlockPos;
import net.minecraft.network.protocol.game.*;
import net.minecraft.server.level.ClientInformation;
import net.minecraft.server.level.ServerPlayer;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.CraftServer;
import org.bukkit.craftbukkit.CraftWorld;
import org.bukkit.craftbukkit.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;

import static java.util.UUID.randomUUID;

public final class NpcForHubPlugin extends JavaPlugin implements Listener {
  private static final String TEXTURES = "textures";
  private static final String CONFIG_VERSION = "config.version";
  private static final String CONFIG_NPC_DATA = "npc.data";
  private final List<ServerPlayer> npcList = new ArrayList<>();
  private final HashMap<Integer, String> npcIdServerName = new HashMap<>();

  private List<NpcData> npcData = new ArrayList<>();

  @Override
  public void onEnable() {
    this.getServer().getPluginManager().registerEvents(this, this);
    getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    initConfig();
    Bukkit.getScheduler().scheduleSyncDelayedTask(this, this::createNPC, 100); // hack zodat de wereld geladen is
    Bukkit.getScheduler().scheduleSyncRepeatingTask(this, this::updateNpcRotation, 1, 1);
  }

  private void initConfig() {
    // first add default config
    if (getConfig().get(CONFIG_VERSION) == null) {
      getConfig().set(CONFIG_NPC_DATA, defaultNpc());
      getConfig().set(CONFIG_VERSION, 1);
      saveConfig();
    }
    // load into memory
    npcData = (List<NpcData>) getConfig().get("npc.data", List.class);
  }

  private List<NpcData> defaultNpc() {
    return List.of(
      new NpcData(24.5, 53, -15.5, "ewogICJ0aW1lc3RhbXAiIDogMTYzNzE4MDc5NTEzNCwKICAicHJvZmlsZUlkIiA6ICJhNzdkNmQ2YmFjOWE0NzY3YTFhNzU1NjYxOTllYmY5MiIsCiAgInByb2ZpbGVOYW1lIiA6ICIwOEJFRDUiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvY2E2NTMzNGIzYmRkZGI4YmUyNmViNTE4NGNjNjUyN2U2YWY2Yzc5MTZlMzEwNDczZmZlODUwZDY4YmJjMmIwNiIKICAgIH0KICB9Cn0=", "fJQr8tlvAgSgzI5gbBuwv9FV4fjeNyVNsO1/clo3+9P5cY42g346fygE6ABOlEROmI6kFjgC5B/gv3PjQAoxqs2YOMywYchEx9eltZqn1Cs4L5sktFAALM4RrZuE1R5x+ixg2MvvfW/7DF5gcEhg+Vqw5gnaeayI9QozVxaU+7Ep1p4qP2vwjnU7pSAkzYbcG0m7bBd50WGw2MijYD07oNHvrA/2QIW2he1IRX68yFHTDYzX+TTOyTBawgkr5FU6zLNKYP7SW7N0VkxhqRge68QwNIyq8UnhRY/97ynWWhvRHTkgp1iZNQLHYyLMYIWVIs/U1Ncng+zPNEEif7kFVasWnJihbjszbAfOJl08prYgTf3TOCEPMElRYfTaVQ/OPgv90QfKOGsFO9A46KdgKwd1FkK2awFJW3ZbWOJB5pkS2e2pLlwnk+IjCl3kVpoDBdDmgichEKobwMSTHcVtU1XoRHIE2hhNBXZM/KzWOaINI5WBMurOYneVWBtWKCILvgznNy8uemAjkrrqV8Aeil+pxmq0C5xZPoUW0Q1We8BAEyTiz1Pnzl4XzryogEvuSnu1lVWpAybdawn4vwlCtqAyqrGXW22USdy66Ko0oAyGBvacgfWet4YMc0TLAhcvBwyTyaMgGC0nQvTSJIpAdGK1Ndm9EovLu8xPRw/DPRE=", "Bingo", "bingo"),
      new NpcData(24.5, 53, -9.5, "eyJ0aW1lc3RhbXAiOjE1ODY1NjUyNjgwMjksInByb2ZpbGVJZCI6IjkxZmUxOTY4N2M5MDQ2NTZhYTFmYzA1OTg2ZGQzZmU3IiwicHJvZmlsZU5hbWUiOiJoaGphYnJpcyIsInNpZ25hdHVyZVJlcXVpcmVkIjp0cnVlLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDliNGFlYzM4MTIzYjE5N2EzYTI1MmViOTg0MDRlOTMzNTA2MDQwNjgxOTYxNWM4OWJhZTY3MmQzNmVlMGUyIn19fQ==", "oKAKsCmBmpdonISACBNjiPnKJGPVOWd35Asf2GIsB7KGFYfdu+DNMqQaqq8+napERJVcOZ2awvk5tZOYDb5PVKo3AVkv/RwnmiHJ5k5UdEqswrNTIoiPjGI7eg4SsyC6GN2kUjvMoqv9vHWcCJNFxQ4M7gxbH1JGCPzIa2HCfHm4d2YCFDrWlR1wFMlGHCwc1Gni1Ek1et+gZ4m9YUEFT629EGrpRoDvgtv3w/KANwhAXILFguRvMFbXmBzD2dZfVxDg+AFjRtfas4p1RAQq/5Tcx8sWrlhbeNPRh0Zdo8bQzgWqMcq1DSM2CRuAZuUGNiwCsxks+mYUFVyMW8m6ubfDCFuFPdWFgFEiAD1gpoJ63WqFday1RHfPwa0I4ESGImam0nYLn4NrMYBiAIVz5mkZZFVdJTMScEx4ydLXb/cATNntE05xNl+rT+0b11eE0lSVdafxarH9eoQm3W9N9ywkUIl6ZQjYo8u0fx0YIRR5z99infCIFvn962A94VnMPC8dlopMQOZmSxkSed8itDV0UA9tRadzo4mdqTuo1ajBmwkdE73AjH3RD09hE6COFZKVWYEmzmcBQLqEC81N+UxYdwoQybOagYtMxUyYIN816rYm5QTUycb7CIexINPKueu5Y/6UlG0HqcreoBUD+VthcOw73DLyM17P7Uy7YIc=", "Block Swap", "blockswap"),
      new NpcData(18.5, 53, -6.5, "ewogICJ0aW1lc3RhbXAiIDogMTYxNDM5Njc1MjI0NiwKICAicHJvZmlsZUlkIiA6ICJmMjc0YzRkNjI1MDQ0ZTQxOGVmYmYwNmM3NWIyMDIxMyIsCiAgInByb2ZpbGVOYW1lIiA6ICJIeXBpZ3NlbCIsCiAgInNpZ25hdHVyZVJlcXVpcmVkIiA6IHRydWUsCiAgInRleHR1cmVzIiA6IHsKICAgICJTS0lOIiA6IHsKICAgICAgInVybCIgOiAiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS9mZjYwMmM3NDg2ZmZmZDViZjhkYzNiY2YzYjZkZTFjNTMzM2I0MDU5ZWExMWRjMGUwZWQxYzQ3YjYwMTllMDZkIgogICAgfQogIH0KfQ==", "PSw5AEf4Z3L7vM2HrhjZqhg3ZSGqtlCGJEEV6DYdXUboSBl41oAs929BN5bNrhgjGQt+XxcbIfAp+GYST8HxhJ0u5UtxDgOz2yFEC0QOk6WbBOSztazb3TXl5vro5NbgGeDDgESkSUw9Hx4TisuPrgdD6qHS5BKF3AUI5n00IPnx2Rt4k6M8zr84b1fzHu8g/aHAI80sUa1YbGn50gkjEdCGR4tPWm719l7Hn68QNvbm3lt4WhF62xCCS1KIR7Hjd2D1DG6e1IsCwisUIgtuNM++6fRRxBhpTtQKlovzrznJDsZLcfMD6eodxtDw5GlMcf30QFP/v1eJ0qCZ8Go0wE5aTtgj/cTEolhbBVZu/gdzHAQKSgjnZrw0Uymb7bHpPGkvu5DYcuZJDv/lNrZ9aXiHK1yHvjgUdJLnkU6jBRp+VV6USAMHrWKrdlAFKNf3/LbWAZWdYT1272REgurwMOyte7q02v+gnJoPEwCzoUeIyb7lLFtBX+4X9WsvD2Tj9gX9tNjurLeP2Uqxq0K7sqTv1a3a9Fv085hSTAszQ+jI8WluVmxDpkRNhVAXF0DVDfRzJNH3NPVoMuFQ8rMeMlSEa0M+gSdQYn62+pZYMiqCQUvb2nPvvAEr5kxYFHOATtmRdLSv6Gkl/J866A0YIl4cZFTzUyQ+qDhgSRrBMf0=", "Bende 6", "bende6"),
      new NpcData(12.5, 53, -9.5, "ewogICJ0aW1lc3RhbXAiIDogMTYzNzE4MDkxNDc5NiwKICAicHJvZmlsZUlkIiA6ICI0NmY3N2NjNmQ2MjU0NjEzYjc2NmYyZDRmMDM2MzZhNiIsCiAgInByb2ZpbGVOYW1lIiA6ICJNaXNzV29sZiIsCiAgInNpZ25hdHVyZVJlcXVpcmVkIiA6IHRydWUsCiAgInRleHR1cmVzIiA6IHsKICAgICJTS0lOIiA6IHsKICAgICAgInVybCIgOiAiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS9mNDNkN2EwNGUzMmE2NmY1ZTFiMTM1YzJmODc3ZjAyOTJlYjI4MTZjNTRlZGYyZjQ3NzI1MjIxNmMzNTlkNmQxIgogICAgfQogIH0KfQ==", "nkOKZKCZh+BNV+k9XMCCHzvXqCTn7hLDfsqGMjo4rasRMzEXZPgAV9EFMNZiDOEXeHj/vWzHcVL4YzeirghyZkdi909z+VTYIC6pkB8USKTYRYx5JFhDILOvBHSvk9gHNsma+LSwpPedzE6ew0q2WwlCpbpD3fg/+wr2q2+67PJ3xrhcwwLZ8ypNRulnmS7ry41AlX5H6b1UuvWhz1k4LWPH4hB3KTHJcBeFLZLTUUeFkc2PNkF6u0kcJ0Kw5j6j8M3ockwDLwqRfel0oe6TwBruIP/CH5Zbggnm3MpGZWQE7b8kjfUhCDSGMDBX29ODf9/eJMo86k/SCuVj+UQfvz11El4dsSD9ck0fGIEkke6lN8YvB+BKGatvM0AGMXOfFmsAHgNcoR8lXeBfoTaeqwR1dPWoxu03EV0x+jSQO/M28Bqsj86FAOzn7vAc+wuQ/9TMD2dnrtts1lu4MWyyd3U/tv8oMsB1GpCfxMT1TUGYJIWfjI8QvtPPrERKwjM6Y694WQEd7qVGuOEAbqyNMI/t6/BdC/a4EYV+e9YtFTLkgrOTd/3DjbSqsRkdcGJbJGBVBtY/Yls1u5U3qqLQE5fScC9Z8pcr3oUSqu1/F7JR9Zx4ngtUtK5VqAxsfAJb1qb7gClV6Q+EYeJVKSTe+6ZToNw5ClOUMEWvqJOR99c=", "Bende 5", "bende5"),
      new NpcData(12.5, 53, -15.5, "ewogICJ0aW1lc3RhbXAiIDogMTYzNzE4MDk1OTE0MiwKICAicHJvZmlsZUlkIiA6ICI3ZGEyYWIzYTkzY2E0OGVlODMwNDhhZmMzYjgwZTY4ZSIsCiAgInByb2ZpbGVOYW1lIiA6ICJHb2xkYXBmZWwiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTU5MTMzYjdmZDQ4MGZmNmJkOWQxNGIxMGIyOTY3MzgxMzQzNWNlZTRkNzMwNTA0OWM0M2I5YzJmNzBlN2RiNSIKICAgIH0KICB9Cn0=", "KTRRpjBFy7mnLiXmrp1RD7mQpoyqowRAdSp4KUIULgjDAaYq25FUhwhxk+XkJsZUYKFdyuNowHJZf3m8L18KOzYOHQuY17E/Uznzg3SQCrXhaXeuv0JBzvnisBGEBB32Ckv4L1IACiIwdT+GXWqbydhoxWigqweEc5ccsuG4251IHq16iDCNeAledptQGmATwXbRq3PVZOnM76jlkheqBZgy5U4KqRO7U7rhomuLYVEyC38GZBDZjRZcscVB/aC1ErlR4sPnxxH2YeqPzeSVST4r/Tz/hvu5c+C2VonfnGzhcLjfxjzH5B9yCKLqVitdVbc850/QVTJLZza3D5r4CuNSe5O6AfZuklIBKOLifUTg/xgoYozYNobnFGTa7K7GNPZVTbRNpGKVPfZzuJ6b0uEe5K1yFtDvjOrBTc1501UyuplrTg1qdxIehpt+5XtKQDu555Ek5IUnesDZhgpwS28969HU0WGcEIAhF8HpBSMQomS5lRmLnv7fTNITjCmkiAiU/NGlc2gs4aGyOF3Gk+sR9kzsjgn5p25lDbbM8cf0vdx9L/c/XgHT8F+J0/IcOCh7i5FEEsU+Wf2sKV2OMaWD82/gjzuyDpyDkWRGKRGwW2iz61RJ6y2arZ4xV+SGwbcpbI24EJ8CRnJj/09oloADOYz2T0PdqWoUxmo93eM=", "Crea", "crea"),
      new NpcData(18.5, 53, -18.5, "ewogICJ0aW1lc3RhbXAiIDogMTYzNzE4MDk5NzIxMiwKICAicHJvZmlsZUlkIiA6ICI0OWIzODUyNDdhMWY0NTM3YjBmN2MwZTFmMTVjMTc2NCIsCiAgInByb2ZpbGVOYW1lIiA6ICJiY2QyMDMzYzYzZWM0YmY4IiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2IxNDdmZGU0NGZmMzFkMTJhMzFiNTlkNTY3OTA0Y2FkYjk4NDY0ZmEyYjdjYjAyMDU5YWEwZDdlMzc5ZmRhZjAiCiAgICB9CiAgfQp9", "Qum2H70/x0ayn7ujaMr/ddp1VkyBwwXR8L2g5HdqbfpTRnKxtIsbNt/AJWvsfeFI41mxv7xq214m2LoOVaE867zD6nYfjVOLxJ7bHylRLlicA4V2WPbYOATQf5eTQzQ2Qo+MoK4nv/u7AeYU8Pizr7yYPONDeBP7gO2oTvw3cLAa9wlBv/jMHA3cVP6PV+H8TaMB3fQvJhx99H+YdtslgY/whgB3YpOllDIvu/3sn12XwR6iVityRHJVgZnCcDg3CrwtBMuqu9tb0deA07FjimYxtXcO7Ax98JPNgtf6G+Oo921BDqbYXUvj1GTfsi2o3cr4w3Rc8kAn92trD27uPSZEp5hNtOCMsGU2dlPXblnCGnhPSM/+8v3buHmUGzGnw3TmvuD77wjsMUj0X8a7j4HHtitZQI0I9SFVOdWUTr5cz7LAHuZDmL4lmvT5O/pcJL4sfAfbr9Mdck776ewHu27MaOVeE4FvogT3yEH1/Ok2I+zja2+ag7Kzjibz1W+hufH6Q+LVt2UyWMcYLnHDTxQgw3/pwnHLLjKzSJ0HCCd94S3ygO+e2stn6OZLI7ZyyyTdr5GYbvWKWZHhpXZQ4L57F/VuoDTj3m55nIvFyXtJnJRiLnvkpmxtiG0/hI3AsCPEoyF4d7mHjo9y+jmhLTzPe5uIe6pMYjWxN4NtjDA=", "Familie", "familie3")
    );
  }


  private void updateNpcRotation() {
    if (npcList.isEmpty())
      return;
    npcList.forEach(npc -> {
      for (Player p : Bukkit.getOnlinePlayers()) {
        if (calculateDistance(p, npc) > 10) continue;

        var connection = ((CraftPlayer) p).getHandle().connection;
        var difference = p.getLocation().subtract(npc.getBukkitEntity().getLocation()).toVector().normalize();
        var degrees = (float) Math.toDegrees(Math.atan2(difference.getZ(), difference.getX()) - Math.PI / 2);
        var angle = (byte) ((degrees * 256.0F) / 360.0F);

        var height = npc.getBukkitEntity().getLocation().subtract(p.getLocation()).toVector().normalize();
        var pitch = (byte) ((Math.toDegrees(Math.atan(height.getY())) * 256.0F) / 360.0F);

        connection.send(new ClientboundRotateHeadPacket(npc, angle));
        connection.send(new ClientboundMoveEntityPacket.Rot(npc.getId(), angle, pitch, true));
      }
    });
  }

  @EventHandler
  public void clickOnNpc(PlayerUseUnknownEntityEvent playerInteractEntityEvent) {
    var player = playerInteractEntityEvent.getPlayer();
    var server = npcIdServerName.get(playerInteractEntityEvent.getEntityId());
    if (server != null) {
      ByteArrayDataOutput out = ByteStreams.newDataOutput();
      out.writeUTF("Connect");
      out.writeUTF(server);
      player.sendPluginMessage(this, "BungeeCord", out.toByteArray());
    }
  }

  @EventHandler
  public void onJoin(PlayerJoinEvent joinEvent) {
    npcList.forEach(this::addNPCPacket);
  }

  private double calculateDistance(Player p, ServerPlayer npc) {
    var diffX = npc.getX() - p.getLocation().getX();
    var diffZ = npc.getZ() - p.getLocation().getZ();
    var x = diffX < 0 ? (diffX * -1) : diffX;
    var z = diffZ < 0 ? (diffZ * -1) : diffZ;
    return Math.sqrt(Math.pow(x, 2) + Math.pow(z, 2));
  }

  public void createNPC() {
    var server = ((CraftServer) Bukkit.getServer()).getServer();
    var world = ((CraftWorld) Bukkit.getWorld("world")).getHandle(); // Change "world" to the world the NPC should be spawned in.
    npcData.forEach(npcData1 -> {
      var gameProfile = createProfiel(npcData1.naam());
      var npc = new ServerPlayer(server, world, gameProfile, ClientInformation.createDefault());
      // todo the npc does not stand correct on the block
      npc.setPos(npcData1.x(), npcData1.y(), npcData1.z());
      setSkinn(gameProfile, npcData1.skinValue(), npcData1.skinSignature());
      addNPCPacket(npc);
      npcList.add(npc);
      npcIdServerName.put(npc.getId(), npcData1.server());
    });
  }

  public GameProfile createProfiel(String naam) {
    return new GameProfile(randomUUID(), naam);
  }

  private void setSkinn(GameProfile gameProfile, String value, String signature) {
    gameProfile.getProperties().get(TEXTURES).clear();
    gameProfile.getProperties().put(TEXTURES, new Property(TEXTURES, value, signature));
  }

  public void addNPCPacket(ServerPlayer npc) {
    Bukkit.getOnlinePlayers().forEach(player -> {
      var connection = ((CraftPlayer) player).getHandle().connection;
      // Enable second layer of skin (https://wiki.vg/Entity_metadata#Player)
      npc.getEntityData().set(net.minecraft.world.entity.player.Player.DATA_PLAYER_MODE_CUSTOMISATION, (byte) (0x01 | 0x02 | 0x04 | 0x08 | 0x10 | 0x20 | 0x40));
      connection.send(new ClientboundPlayerInfoUpdatePacket((EnumSet.of(ClientboundPlayerInfoUpdatePacket.Action.ADD_PLAYER)), getEntry(npc)));
      connection.send(new ClientboundAddEntityPacket(npc, 0, BlockPos.containing(npc.getEyePosition()).below(1)));
      var dataValues = npc.getEntityData().getNonDefaultValues();
      assert dataValues != null;
      connection.send(new ClientboundSetEntityDataPacket(npc.getId(), dataValues));
    });
  }

  private ClientboundPlayerInfoUpdatePacket.Entry getEntry(ServerPlayer npcPlayer) {
    return new ClientboundPlayerInfoUpdatePacket.Entry(
      npcPlayer.getUUID(),
      npcPlayer.getGameProfile(),
      false,
      69,
      npcPlayer.gameMode.getGameModeForPlayer(),
      npcPlayer.getTabListDisplayName(),
      null
    );
  }
}
